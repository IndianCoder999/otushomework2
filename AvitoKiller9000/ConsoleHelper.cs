﻿using AvitoKiller9000.Entities;
using AvitoKiller9000.Repository;

namespace AvitoKiller9000;

public static class ConsoleHelper
{
    public static void Menu(
        string? title = null,
        string? prompt = null,
        string? invalidMessage = null,
        params (string DisplayName, Action Action)[] options)
    {
        // handling parameters
        if (options is null)
        {
            throw new ArgumentNullException(nameof(options));
        }

        if (options.Length <= 0)
        {
            throw new ArgumentException($"{options} is empty", nameof(options));
        }

        title ??= "Menu...";
        prompt ??= $"Choose an option (1-{options.Length}): ";
        invalidMessage ??= "Invalid Input. Try Again...";

        // render menu
        Console.WriteLine(title);
        for (int i = 0; i < options.Length; i++)
        {
            Console.WriteLine($"{i + 1}. {options[i].DisplayName}");
        }

        // get user input
        int inputValue;
        Console.Write(prompt);
        while (!int.TryParse(Console.ReadLine(), out inputValue) || inputValue < 1 || options.Length < inputValue)
        {
            Console.WriteLine(invalidMessage);
            Console.Write(prompt);
        }

        Console.WriteLine();

        // invoke the action relative to the user input
        options[inputValue - 1].Action.Invoke();
    }

    public static object GetObjectFromUser(Type type, AvitoRepository avitoRepository)
    {
        // Создаем с помощью консоли объекты любых типов
        
        // Создаем объект
        var instance = Activator.CreateInstance(type);

        // Заполняем поля информацией от пользователя
        foreach (var property in type.GetProperties().Where(p => p.Name != "Id"))
        {
            if (property.PropertyType.IsSubclassOf(typeof(Entity)))
            {
                // Если это суррогатный ключ, то нужно получить от пользователя Id и убедиться, что такой объект есть в базе
                object? entity = null;
                while (entity == null)
                {
                    Console.WriteLine($"Enter {property.Name} id (Foreign key):");
                    var valueFromUser = Console.ReadLine();
                    if (int.TryParse(valueFromUser, out var id))
                    {
                        entity = avitoRepository.GetEntityWithTypeAndId(property.PropertyType, id);
                    }
                }
                property.SetValue(instance, entity);
            }
            else
            {
                // Если это просто поле, то парсим информацию от пользователя в любом типе
                object? castedValue = null;
                while (castedValue == null)
                {
                    Console.WriteLine($"Enter {property.Name} ({property.PropertyType.Name}):");
                    var valueFromUser = Console.ReadLine();
                    castedValue = Convert.ChangeType(valueFromUser, property.PropertyType);
                }
                property.SetValue(instance, castedValue);
            }
        }

        return instance;
    }
}