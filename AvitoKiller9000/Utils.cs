﻿using AvitoKiller9000.Mappings;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace AvitoKiller9000;

public class Utils
{
    private const string ConnectionString = "Host=localhost;Username=postgres;Password=superuser666;Database=AvitoKiller9000";

    public static ISessionFactory CreateSessionFactory()
    {
        var sessionFactory = Fluently.Configure()
                .Database(PostgreSQLConfiguration.Standard.ConnectionString(ConnectionString))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>())
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                .BuildSessionFactory();

        return sessionFactory;
    }
}