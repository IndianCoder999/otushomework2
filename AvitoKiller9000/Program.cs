﻿using AvitoKiller9000;
using AvitoKiller9000.Entities;
using AvitoKiller9000.Repository;


var tables = new List<(string tableName, Type type)>
{
    ("Users", typeof(User)),
    ("Ads", typeof(Ad)),
    ("Ad orders", typeof(AdOrder)),
    ("Ads in wishlist", typeof(AdInWishList))
};

var sessionFactory = Utils.CreateSessionFactory();
var repository = new AvitoRepository(sessionFactory);
RootMenu();


void RootMenu()
{
    ConsoleHelper.Menu("Welcome to AvitoKiller9000", invalidMessage: "Invalid input", prompt: "Choose what to do: ",
        options: new (string, Action)[]
        {
            ("Add to table", AddToSomeTableMenu),
            ("Show tables", ShowAll),
            ("Add Vasya", AddVasya)
        });
}


void AddToSomeTableMenu()
{
    ConsoleHelper.Menu("Add to table", invalidMessage: "Invalid input", prompt: "Choose table to add to: ",
        options: tables.Select(t => (t.tableName, new Action(() => AddNewObjectToTable(t.type)))).ToArray());
}

void ShowAll()
{
    // Я не стал делать красивое оформление, т.к. ДЗ не про это
    foreach (var (tableName, type) in tables)
    {
        Console.WriteLine($"{tableName}:");
        Console.WriteLine(repository.GetAllEntitiesInOneString(type));
        Console.WriteLine();
    }

    RootMenu();
}

void AddVasya()
{
    var vasya = new User
    {
        Email = "vasya@mail.ru",
        FirstName = "Vasya",
        LastName = "Morozov",
        MiddleName = "Vadimovich"
    };

    var ad = new Ad
    {
        User = vasya,
        Address = "Moscow",
        Title = "Good chair",
        Description = "Sell a chair",
        PriceRub = 2000,
        PhoneNumber = "+7-666-666-66-66"
    };

    var adInWishList = new AdInWishList
    {
        Ad = ad,
        User = vasya
    };

    var adOrder = new AdOrder
    {
        Ad = ad,
        Address = "Moscow, Kremlyevskaya street, 111",
        Completed = false,
        User = vasya
    };

    repository.SaveOrUpdate(vasya);
    repository.SaveOrUpdate(ad);
    repository.SaveOrUpdate(adInWishList);
    repository.SaveOrUpdate(adOrder);
    
    Console.WriteLine();
    RootMenu();
}

void AddNewObjectToTable(Type type)
{
    var objectFromUser = ConsoleHelper.GetObjectFromUser(type, repository);
    repository.SaveOrUpdate(objectFromUser);
    Console.WriteLine();
    RootMenu();
}