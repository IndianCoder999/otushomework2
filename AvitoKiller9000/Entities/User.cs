﻿namespace AvitoKiller9000.Entities;

public class User : Entity
{
    public virtual string FirstName { get; set; }
    public virtual string LastName { get; set; }
    public virtual string MiddleName { get; set; }
    public virtual string Email { get; set; }

    public override string ToString()
    {
        return $"{Id} {FirstName} {LastName} {MiddleName} {Email}";
    }
}