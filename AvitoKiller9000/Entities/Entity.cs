﻿namespace AvitoKiller9000.Entities;

public abstract class Entity
{
    public virtual long Id { get; set; }

}