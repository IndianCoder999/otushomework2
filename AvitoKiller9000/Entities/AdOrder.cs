﻿namespace AvitoKiller9000.Entities;

public class AdOrder : Entity
{
    public virtual Ad Ad { get; set; }
    public virtual User User { get; set; }
    public virtual string Address { get; set; }
    public virtual bool Completed  { get; set; }

    public override string ToString()
    {
        return $"{Id} {Ad} {User} {Address} {Completed}";
    }
}