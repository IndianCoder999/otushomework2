﻿namespace AvitoKiller9000.Entities;

/// <summary>
/// Объявление
/// </summary>
public class Ad : Entity
{
    public virtual User User { get; set; }
    public virtual string Title { get; set; }
    public virtual string Description { get; set; }
    public virtual string Address { get; set; }
    public virtual string PhoneNumber { get; set; }
    public virtual decimal PriceRub { get; set; }

    public override string ToString()
    {
        return $"{Id} {User.FirstName} {Title} {Description} {Address} {PhoneNumber} {PriceRub}"; 
    }
}