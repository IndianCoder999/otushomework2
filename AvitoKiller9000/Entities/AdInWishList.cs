﻿namespace AvitoKiller9000.Entities;

/// <summary>
/// Объявление, сохраненное в "Избранном"
/// (Связь объявлений и пользователей "Многое ко многому")
/// </summary>
public class AdInWishList : Entity
{
    /// <summary>
    /// Объявление
    /// </summary>
    public virtual Ad Ad { get; set; }
    /// <summary>
    /// Пользователь, который сохранил его у себя
    /// </summary>
    public virtual User User { get; set; }

    public override string ToString()
    {
        return $"{Id} {Ad.Id} {User.Id}";
    }
}