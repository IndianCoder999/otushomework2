﻿using System.Text;
using AvitoKiller9000.Entities;
using NHibernate;

namespace AvitoKiller9000.Repository;

public class AvitoRepository
{
    private readonly ISessionFactory _sessionFactory;

    public AvitoRepository(ISessionFactory sessionFactory)
    {
        _sessionFactory = sessionFactory;
    }

    public string GetAllEntitiesInOneString(Type type)
    {
        using var session = _sessionFactory.OpenSession();
        var queryable = QueryByType(type, session);
        var sb = new StringBuilder();
        foreach (var o in queryable)
        {
            sb.Append(o);
            sb.AppendLine();
        }
        return sb.ToString();
    }

    public object? GetEntityWithTypeAndId(Type type, int id)
    {
        using var session = _sessionFactory.OpenSession();
        return QueryByType(type, session)
            .FirstOrDefault(o => ((Entity)o).Id == id);
    }

    public void SaveOrUpdate(object obj)
    {
        using var session = _sessionFactory.OpenSession();
        using var tx = session.BeginTransaction();
        session.SaveOrUpdate(obj);
        tx.Commit();
    }

    private IQueryable<object> QueryByType(Type type, ISession session)
    {
       return ((IQueryable<object>)session.GetType()
            .GetMethods()
            .FirstOrDefault(method => method.Name == "Query" && method.GetParameters().Count() == 0)!
            .MakeGenericMethod(type)
            .Invoke(session, Array.Empty<object>())!);

    }
}