﻿using AvitoKiller9000.Entities;
using FluentNHibernate.Mapping;

namespace AvitoKiller9000.Mappings;

public class UserMap : ClassMap<User>
{
    public UserMap()
    {
        Id(x => x.Id);
        Map(x => x.FirstName).CustomSqlType("text").Not.Nullable();
        Map(x => x.LastName).CustomSqlType("text").Not.Nullable();
        Map(x => x.MiddleName).CustomSqlType("text");
        Map(x => x.Email).Index("user_email").Unique().CustomSqlType("text").Not.Nullable();
    }
}