﻿using AvitoKiller9000.Entities;
using FluentNHibernate.Mapping;

namespace AvitoKiller9000.Mappings;

public class AdInWishListMap : ClassMap<AdInWishList>
{
    public AdInWishListMap()
    {
        Id(m => m.Id);
        References(m => m.Ad).Not.Nullable();
        References(m => m.User).Not.Nullable();
    }
}