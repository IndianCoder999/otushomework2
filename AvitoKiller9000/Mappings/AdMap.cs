﻿using AvitoKiller9000.Entities;
using FluentNHibernate.Mapping;

namespace AvitoKiller9000.Mappings;

public class AdMap : ClassMap<Ad>
{
    public AdMap()
    {
        Id(x => x.Id);
        References(x => x.User).Not.Nullable();
        Map(x => x.Title).Not.Nullable();
        Map(x => x.Description).Not.Nullable();
        Map(x => x.Address);
        Map(x => x.PhoneNumber);
        Map(x => x.PriceRub);
    }
}