﻿using AvitoKiller9000.Entities;
using FluentNHibernate.Mapping;

namespace AvitoKiller9000.Mappings;

public class AdOrderMap : ClassMap<AdOrder>
{
    public AdOrderMap()
    {
        Id(m => m.Id);
        References(m => m.Ad).Not.Nullable();
        References(m => m.User).Not.Nullable();
        Map(m => m.Address);
        Map(m => m.Completed);
    }
}